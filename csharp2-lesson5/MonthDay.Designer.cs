﻿namespace csharp2_lesson5
{
    partial class MonthDay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monthcombobox = new System.Windows.Forms.ComboBox();
            this.monthlabel = new System.Windows.Forms.Label();
            this.gobutton = new System.Windows.Forms.Button();
            this.monthslabel = new System.Windows.Forms.Label();
            this.nummonthslabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // monthcombobox
            // 
            this.monthcombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.monthcombobox.FormattingEnabled = true;
            this.monthcombobox.Items.AddRange(new object[] {
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec"});
            this.monthcombobox.Location = new System.Drawing.Point(112, 33);
            this.monthcombobox.Name = "monthcombobox";
            this.monthcombobox.Size = new System.Drawing.Size(121, 21);
            this.monthcombobox.TabIndex = 0;
            // 
            // monthlabel
            // 
            this.monthlabel.AutoSize = true;
            this.monthlabel.Location = new System.Drawing.Point(12, 36);
            this.monthlabel.Name = "monthlabel";
            this.monthlabel.Size = new System.Drawing.Size(70, 13);
            this.monthlabel.TabIndex = 1;
            this.monthlabel.Text = "Select Month";
            // 
            // gobutton
            // 
            this.gobutton.Location = new System.Drawing.Point(112, 61);
            this.gobutton.Name = "gobutton";
            this.gobutton.Size = new System.Drawing.Size(75, 23);
            this.gobutton.TabIndex = 2;
            this.gobutton.Text = "Go";
            this.gobutton.UseVisualStyleBackColor = true;
            this.gobutton.Click += new System.EventHandler(this.gobutton_Click);
            // 
            // monthslabel
            // 
            this.monthslabel.AutoSize = true;
            this.monthslabel.Location = new System.Drawing.Point(112, 122);
            this.monthslabel.Name = "monthslabel";
            this.monthslabel.Size = new System.Drawing.Size(0, 13);
            this.monthslabel.TabIndex = 3;
            // 
            // nummonthslabel
            // 
            this.nummonthslabel.AutoSize = true;
            this.nummonthslabel.Location = new System.Drawing.Point(15, 121);
            this.nummonthslabel.Name = "nummonthslabel";
            this.nummonthslabel.Size = new System.Drawing.Size(94, 13);
            this.nummonthslabel.TabIndex = 4;
            this.nummonthslabel.Text = "Number of Months";
            // 
            // MonthDay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.nummonthslabel);
            this.Controls.Add(this.monthslabel);
            this.Controls.Add(this.gobutton);
            this.Controls.Add(this.monthlabel);
            this.Controls.Add(this.monthcombobox);
            this.Name = "MonthDay";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox monthcombobox;
        private System.Windows.Forms.Label monthlabel;
        private System.Windows.Forms.Button gobutton;
        private System.Windows.Forms.Label monthslabel;
        private System.Windows.Forms.Label nummonthslabel;
    }
}

