﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp2_lesson5
{
    public partial class MonthDay : Form
    {
        public MonthDay()
        {
            InitializeComponent();
        }

        private void gobutton_Click(object sender, EventArgs e)
        {
            string month = monthcombobox.Text;
            monthslabel.Text = Convert.ToString(getDaysInMonth(month));
           
        }

        private int getDaysInMonth(string m)
        {
            string mth = m;
            switch (mth)
            {
                case "Jan":
                    return 31;
                    
                case "Feb":
                    return 28;
                    
                case "Mar":
                    return 31;
                    
                case "Apr":
                    return 30;
                    
                case "May":
                    return 31;
                    
                case "Jun":
                    return 30;
                    
                case "Jul":
                    return 31;
                    
                case "Aug":
                    return 31;
                    
                case "Sep":
                    return 30;
                    
                case "Oct":
                    return 31;
                    
                case "Nov":
                    return 30;
                    
                case "Dec":
                    return 31;
                    
                default:
                    return 0;
                    
            }
        }
    }
}
